<?php

/**
 * Registers the stylesheet for the example template.
 *
 * @uses wp_enqueue_style
 * @return void
 */

add_action( 'wp_enqueue_scripts', 'rcno_example_styles' );
add_action( 'wp_enqueue_scripts', 'rcno_example_scripts' );

function rcno_example_styles() {
	wp_enqueue_style( 'rcno-example-style', plugin_dir_url( __FILE__ ) . 'example-style.css', array(), '1.0.0', 'all' );
}

function rcno_example_scripts() {
	wp_enqueue_script( 'rcno-example-script', plugin_dir_url( __FILE__ ) . 'example-script.js', array( 'jquery' ), '1.0.0', true );
}
