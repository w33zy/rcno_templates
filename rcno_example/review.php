<?php
/*
Author: wzyMedia
Author Mail: kemory@wzymedia.com
Author URL: https://wzymedia.com
Layout Name: Rcno Example
Version: 1.0.0
Description: An example review layout.
*/

// Get the review ID.
$review_id = ! empty( $GLOBALS['review_id'] ) ? $GLOBALS['review_id'] : get_post()->ID;

// Get an instance of the template class.
$template = new Rcno_Template_Tags( 'rcno-reviews', '1.38.0' );

// If the review is embedded this method handles the review title.
$template->the_rcno_review_title( $review_id );

do_action( 'before_rcno_book_review' ); 
?>

<div class="example-template-container">

  <?php $template->the_rcno_book_review_content( $review_id ); ?>

</div>

<?php do_action( 'after_rcno_book_review' );
